$(document).ready(function() {
    var map;

    var chisinauCenter = new google.maps.LatLng(47.01884, 28.83868);

    var foxIcon = {
        url: 'fox.png',
        size: new google.maps.Size(41, 37),
        origin: new google.maps.Point(0, 0),
    };

    var chickenIcon = {
        url: 'chicken.png',
        size: new google.maps.Size(27, 27),
        origin: new google.maps.Point(0, 0),
    };

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: chisinauCenter,
        mapTypeId: google.maps.MapTypeId.HYBRID
    });

    var drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.POLYGON,
        drawingControl: true,
        drawingControlOptions: {
            position: google.maps.ControlPosition.BOTTOM_CENTER,
            drawingModes: ['marker', 'polygon']
        },
        markerOptions: {
            icon: chickenIcon,
            animation: google.maps.Animation.BOUNCE
        },
        polygonOptions: {
            fillColor: '#ffff00',
            fillOpacity: 0.1,
            strokeWeight: 3,
            clickable: true,
            editable: false,
            zIndex: 1
        }
    });
    drawingManager.setMap(map);
    google.maps.event.addListener(drawingManager, 'polygoncomplete', function(poly) {
        drawingManager.setDrawingMode(null);
        drawingManager.setOptions({
            drawingControlOptions: {
                drawingModes: ['marker'],
                position: google.maps.ControlPosition.BOTTOM_CENTER
            }
        });

        var chickens = [];

        google.maps.event.addListener(drawingManager, 'markercomplete', function(chicken) {
            chickens.push(chicken);
        });

        google.maps.event.addListener(poly, 'click', function(object) {

            var fox = new google.maps.Marker({
                position: object.latLng,
                map: map,
                icon: foxIcon,
                draggable: false
            });

            var steps = [{
                    "stepLatitude": 0.001,
                    "stepLong": 0
                },
                {
                    "stepLatitude": -0.001,
                    "stepLong": 0
                },
                {
                    "stepLatitude": 0.001,
                    "stepLong": 0.001
                },
                {
                    "stepLatitude": -0.001,
                    "stepLong": -0.001
                },
                {
                    "stepLatitude": 0,
                    "stepLong": 0.001
                },
                {
                    "stepLatitude": 0,
                    "stepLong": -0.001
                },
                {
                    "stepLatitude": -0.001,
                    "stepLong": 0.001
                },
                {
                    "stepLatitude": 0.001,
                    "stepLong": -0.001
                }
            ];
            var index = 4;

            function hunt() {
                var latitude = fox.getPosition().lat();
                var longitude = fox.getPosition().lng();
                var newPosition = new google.maps.LatLng(latitude + steps[index].stepLatitude, longitude + steps[index].stepLong);
                var isFoxInsideCoop = google.maps.geometry.poly.containsLocation(newPosition, poly);
                if (isFoxInsideCoop) {
                    fox.setPosition(newPosition);
                    startHunting();
                } else {
                    index = (index + randomInt(1, 10)) % 8;
                    hunt();
                }
            };

            function randomInt(min, max) {
                min = Math.ceil(min);
                max = Math.floor(max);
                var randomInt = Math.floor(Math.random() * (max - min)) + min;
                return randomInt;
            };

            var huntInterval = setInterval(hunt, 20);
            
            var capturedChicken;

            function startHunting() {
                var i = 0;
                var limitDistance = 200;
                var foxLocation = fox.getPosition();
                for (i = 0; i < chickens.length; i++) {
                    var foxChickenDistance = google.maps.geometry.spherical.computeDistanceBetween(chickens[i].getPosition(), foxLocation);
                    if (foxChickenDistance < limitDistance ) {
                        finishHunting(chickens[i]);
                    };
                };
            };

            function finishHunting(capture) {
                clearInterval(huntInterval);
                capturedChicken = capture;
                setInterval(leaveCoop,5);
            };
            
            function leaveCoop(){
                var foxLatitude = fox.getPosition().lat();
                var foxLongitude = fox.getPosition().lng();
                var foxNewPosition = new google.maps.LatLng(foxLatitude + steps[index].stepLatitude, foxLongitude + steps[index].stepLong);
                fox.setPosition(foxNewPosition);

                var chickenLatitude = capturedChicken.getPosition().lat();
                var chickenLongitude = capturedChicken.getPosition().lng();
                var chickenNewPosition = new google.maps.LatLng(chickenLatitude + steps[index].stepLatitude, 
                    chickenLongitude + steps[index].stepLong);
                
                capturedChicken.setPosition(chickenNewPosition);
            };
        });
    });
});