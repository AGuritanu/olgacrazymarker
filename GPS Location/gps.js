$(document).ready(function(){
    
    var map;
    var mapCenter = new google.maps.LatLng(47.01884, 28.83868);
    map = new google.maps.Map(document.getElementById("map"),{
        zoom: 12,
        center: mapCenter,
        mapTypeId: google.maps.MapTypeId.HYBRID
    });

    var myLocation = new google.maps.Marker ({
        position: mapCenter,
        map: map,
        draggable: false
    });
    myLocation.setMap(map);

    $(".button").click(function() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showLocation, showError);
        } else {
            alert("Your browser does not suport geolocation");
          } 
    });

    function showLocation(position){
        var myPosition = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
        };
        myLocation.setPosition(myPosition);
        myLocation.setMap(map);
        map.setCenter(myPosition);
        map.setZoom(16);
    }

    function showError(error) {
        switch(error.code) { 
            case error.PERMISSION_DENIED:
                alert("User denied the request for Geolocation."); break;
            case error.POSITION_UNAVAILABLE:
                alert("Location information is unavailable."); break;
            case error.TIMEOUT:
                alert("The request to get user location timed out."); break;
            case error.UNKNOWN_ERROR:
                alert("An unknown error occurred."); break;
        }
    } 

});